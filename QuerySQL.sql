create database ComparadorCelulares;

use ComparadorCelulares;

create table Fabricante (
	cod_fab int identity PRIMARY KEY,
	nome_fab varchar(20),
	pais_fab varchar(30),
);

create table Funcionalidade (
	cod_func int identity PRIMARY KEY,
	nome_func varchar(40),
);

create table Celular (
	cod_cel int identity PRIMARY KEY,
	cod_fab int references Fabricante(cod_fab),
	nome_cel varchar(30),
	tela_cel float,
	foto varchar(100),
	flash varchar(4),
);

create table FuncDoCelular (
	cod_funcCel int identity PRIMARY KEY,
	cod_cel int references Celular(cod_cel),
	cod_func int references Funcionalidade(cod_func),
);

select * from Celular
select * from Fabricante
select * from Funcionalidade

Select nome_fab from Fabricante

insert into Fabricante values ('Apple','USA')
insert into Celular values (1,'iPhone',6.7,'linkfoto','Sim')
insert into Celular values ((select cod_fab from Fabricante where nome_fab = 'Apple'), 'iPhone 11 Pro', 6.9, 'linkdafotinha','Sim')
insert into FuncDoCelular values ((select cod_cel from Celular where nome_cel = 'iPhone 11 Pro'), (select cod_func from Funcionalidade where nome_func = 'Bussola'))
insert into FuncDoCelular values (3,4)

Select cod_fab, nome_fab, pais_fab from Fabricante

select C.cod_cel, F.nome_fab, C.nome_cel, C.tela_cel, C.foto, C.flash from Celular C, Fabricante F where C.cod_fab = F.cod_fab

select pais_fab from Fabricante where nome_fab = 'Apple'

select C.cod_cel, FA.nome_fab, C.nome_cel, C.tela_cel, C.foto, C.flash, F.nome_func from Celular C, Funcionalidade F, FuncDoCelular FC, Fabricante FA where C.cod_cel = FC.cod_cel and F.cod_func = FC.cod_func and FA.cod_fab = C.cod_fab
select F.nome_func from Funcionalidade F, Celular C, FuncDoCelular FC where F.cod_func = FC.cod_func and FC.cod_cel = C.cod_cel and C.nome_cel = 'iPhone 11'

select * from FuncDoCelular

delete from FuncDoCelular
delete from Celular 
