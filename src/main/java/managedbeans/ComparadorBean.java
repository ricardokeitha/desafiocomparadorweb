package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ComparadorBean {
	
	private String fabricanteComp;

	public String getFabricanteComp() {
		return fabricanteComp;
	}

	public void setFabricanteComp(String fabricanteComp) {
		this.fabricanteComp = fabricanteComp;
	}

}
