package managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import dao.FabricantesDAO;
import entities.Fabricante;

@ManagedBean
@SessionScoped
public class FabricantesBean {
	
	private Fabricante fabricante = new Fabricante();
	private List<Fabricante> fabricantes = new ArrayList<Fabricante>();
	private FabricantesDAO fabrDAO = new FabricantesDAO();
	private List<SelectItem> listafabr = new ArrayList<SelectItem>();
	private String paisf;
	
	public String paisDaFabricante(String a) {
		paisf = fabrDAO.paisfab(a);
		return paisf;
	}

	public void adicionar()
	{
		fabricantes.add(fabricante);
		fabrDAO.salvar(fabricante);
		fabricante = new Fabricante();
	}
	
	public void listar() {
		fabricantes = fabrDAO.buscar();
	}
	
	public void listarFab() {
		listafabr = fabrDAO.listfabr();
	}

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}

	public List<Fabricante> getFabricantes() {
		return fabricantes;
	}

	public void setFabricantes(List<Fabricante> fabricantes) {
		this.fabricantes = fabricantes;
	}

	public List<SelectItem> getListafabr() {
		return listafabr;
	}
	
	
}
