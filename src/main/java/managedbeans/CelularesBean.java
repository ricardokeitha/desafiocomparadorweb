package managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import entities.Celular;
import dao.CelularesDAO;

@ManagedBean
@SessionScoped
public class CelularesBean {
	
	private Celular celular = new Celular();
	private List<Celular> celulares = new ArrayList<Celular>();
	private CelularesDAO celularDAO = new CelularesDAO();
	private List<SelectItem> listcel = new ArrayList<SelectItem>();
	
	public void adicionar()
	{
		celulares.add(celular);
		celularDAO.salvar(celular);
		celular = new Celular();
	}
	
	public void listar() {
		celulares = celularDAO.buscar();
	}

	public void listarCel() {
		listcel = celularDAO.listcel();
	}

	public Celular getCelular() {
		return celular;
	}

	public void setCelular(Celular celular) {
		this.celular = celular;
	}

	public List<Celular> getCelulares() {
		return celulares;
	}

	public void setCelulares(List<Celular> celulares) {
		this.celulares = celulares;
	}

	public List<SelectItem> getListcel() {
		return listcel;
	}

	public void setListcel(List<SelectItem> listcel) {
		this.listcel = listcel;
	}
	
	
	
}
