package managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import dao.FuncionalidadesDAO;
import entities.Funcionalidade;

@ManagedBean
@SessionScoped
public class FuncionalidadesBean {
	
	private Funcionalidade func = new Funcionalidade();
	private List<Funcionalidade> funcs = new ArrayList<Funcionalidade>();
	private FuncionalidadesDAO funcDAO = new FuncionalidadesDAO();
	private List<SelectItem> listafunc = new ArrayList<SelectItem>();
	private List<String> listaFuncsCel = new ArrayList<String>();
	
	public void adicionar() 
	{
		funcs.add(func);
		funcDAO.salvar(func);
		func = new Funcionalidade();
	}
	
	public void listar() {
		funcs = funcDAO.buscar();
	}
	
	public void listarFunc() {
		listafunc = funcDAO.listfunc();
	}
	
	public void listarFuncsCel(String cel) {
		listaFuncsCel = funcDAO.listFuncsCel(cel);
	}
	
	public Funcionalidade getFunc() {
		return func;
	}
	public void setFunc(Funcionalidade func) {
		this.func = func;
	}
	public List<Funcionalidade> getFuncs() {
		return funcs;
	}
	public void setFuncs(List<Funcionalidade> funcs) {
		this.funcs = funcs;
	}
	public FuncionalidadesDAO getFuncDAO() {
		return funcDAO;
	}
	public void setFuncDAO(FuncionalidadesDAO funcDAO) {
		this.funcDAO = funcDAO;
	}
	public List<SelectItem> getListafunc() {
		return listafunc;
	}
	public void setListafunc(List<SelectItem> listafunc) {
		this.listafunc = listafunc;
	}

	public List<String> getListaFuncsCel() {
		return listaFuncsCel;
	}

	public void setListaFuncsCel(List<String> listaFuncsCel) {
		this.listaFuncsCel = listaFuncsCel;
	}
	
	
}
