package entities;


import java.util.List;

import javax.annotation.PostConstruct;

public class Celular {
	
	private Integer codigo;
	private String fabricante;
	private String modelo;
	private double tela;
	private String foto;
	private String flash;
	private List<String> funcsdocel;

	public List<String> getFuncsdocel() {
		return funcsdocel;
	}
	public void setFuncsdocel(List<String> funcsdocel) {
		this.funcsdocel = funcsdocel;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public double getTela() {
		return tela;
	}
	public void setTela(double tela) {
		this.tela = tela;
	}
	
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	@PostConstruct
	public void init() {
		flash = "Sim";
	}
	
	public String getFlash() {
		return flash;
	}
	public void setFlash(String flash) {
		this.flash = flash;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Celular other = (Celular) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	

}
