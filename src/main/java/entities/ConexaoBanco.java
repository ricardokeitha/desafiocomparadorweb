package entities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoBanco {
	
	private static Connection connection;
	private static final String URL_CONEXAO = "jdbc:sqlserver://localhost;databaseName=ComparadorCelulares;integratedSecurity=true";
	
	public static Connection getConnection() {
		if(connection==null) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				connection = DriverManager.getConnection(URL_CONEXAO);
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}	
		}
		return connection;
	}
	
	public static void fecharConexao()
	{
		if(connection!=null) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException ex) {
				ex.printStackTrace();
			}	
		}
	}
}
