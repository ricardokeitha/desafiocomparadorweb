package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.Celular;
import entities.ConexaoBanco;

public class ComparadorDAO {
	public List<String> buscar() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("select F.nome_fab, C.tela_cel, C.foto, C.flash from Celular C, Fabricante F where C.cod_fab = F.cod_fab");
			ResultSet rs = ps.executeQuery();
			List<String> celulares = new ArrayList<String>();
			while(rs.next()) {
				Celular celular = new Celular();
				celular.setFabricante(rs.getString("nome_fab"));
				celular.setTela(rs.getDouble("tela_cel"));
				celular.setFoto(rs.getString("foto"));
				celular.setFlash(rs.getString("flash"));
			}
			ConexaoBanco.fecharConexao();
			return celulares;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
