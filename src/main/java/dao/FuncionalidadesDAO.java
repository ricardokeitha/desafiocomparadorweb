package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import entities.ConexaoBanco;
import entities.Funcionalidade;

public class FuncionalidadesDAO {
	public void salvar(Funcionalidade func) {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps = connection.prepareStatement("INSERT INTO Funcionalidade (nome_func) VALUES (?) ");
			ps.setString(1, func.getNome());
			ps.execute();
			ConexaoBanco.fecharConexao();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Funcionalidade> buscar() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("Select cod_func, nome_func from Funcionalidade");
			ResultSet rs = ps.executeQuery();
			List<Funcionalidade> funcs = new ArrayList<Funcionalidade>();
			while(rs.next()) {
				Funcionalidade func = new Funcionalidade();
				func.setCodigo(rs.getInt("cod_func"));
				func.setNome(rs.getString("nome_func"));
				funcs.add(func);
			}
			ConexaoBanco.fecharConexao();
			return funcs;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	public List<SelectItem> listfunc() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("Select nome_func from Funcionalidade");
			ResultSet rs = ps.executeQuery();
			List<SelectItem> listafunc = new ArrayList<SelectItem>();
			while(rs.next()) {
				listafunc.add(new SelectItem(rs.getString("nome_func")));
			}
			ConexaoBanco.fecharConexao();
			return listafunc;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	public List<String> listFuncsCel(String cel)
	{
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("select F.nome_func from Funcionalidade F, Celular C, FuncDoCelular FC where F.cod_func = FC.cod_func and FC.cod_cel = C.cod_cel and C.nome_cel = ?");
			ps.setString(1, cel);
			ResultSet rs = ps.executeQuery();
			List<String> listafunc = new ArrayList<String>();
			while(rs.next()) {
				listafunc.add(new String (rs.getString("nome_func")));
			}
			listafunc.add("Nada");
			ConexaoBanco.fecharConexao();
			return listafunc;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
}
