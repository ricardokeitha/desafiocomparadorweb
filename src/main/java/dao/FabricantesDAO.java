package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import entities.ConexaoBanco;
import entities.Fabricante;

public class FabricantesDAO {
	public void salvar(Fabricante fabr) {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps = connection.prepareStatement("INSERT INTO Fabricante (nome_fab, pais_fab) VALUES (?,?) ");
			ps.setString(1, fabr.getNome());
			ps.setString(2, fabr.getPais());
			ps.execute();
			ConexaoBanco.fecharConexao();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Fabricante> buscar() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("Select cod_fab, nome_fab, pais_fab from Fabricante");
			ResultSet rs = ps.executeQuery();
			List<Fabricante> fabrs = new ArrayList<Fabricante>();
			while(rs.next()) {
				Fabricante fabr = new Fabricante();
				fabr.setCodigo(rs.getInt("cod_fab"));
				fabr.setNome(rs.getString("nome_fab"));
				fabr.setPais(rs.getString("pais_fab"));
				fabrs.add(fabr);
			}
			ConexaoBanco.fecharConexao();
			return fabrs;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	public List<SelectItem> listfabr() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("Select nome_fab from Fabricante");
			ResultSet rs = ps.executeQuery();
			List<SelectItem> listafabr = new ArrayList<SelectItem>();
			while(rs.next()) {
				listafabr.add(new SelectItem(rs.getString("nome_fab")));
			}
			ConexaoBanco.fecharConexao();
			return listafabr;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	public String paisfab(String a) {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("Select pais_fab from Fabricante where nome_fab = ?");
			ps.setString(1, a);
			ResultSet rs = ps.executeQuery();
			String result = rs.getString("nome_fab");
			ConexaoBanco.fecharConexao();
			return result;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
