package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import entities.Celular;
import entities.ConexaoBanco;

public class CelularesDAO {
	
	public void salvar(Celular celular) {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps = connection.prepareStatement("INSERT INTO Celular (cod_fab, nome_cel, tela_cel, foto, flash) VALUES ((select cod_fab from Fabricante where nome_fab = ?),?,?,?,?) ");
			ps.setString(1, celular.getFabricante());
			ps.setString(2, celular.getModelo());
			ps.setDouble(3, celular.getTela());
			ps.setString(4, celular.getFoto());
			ps.setString(5, celular.getFlash());
			ps.execute();
			List<String> celFuncs = new ArrayList<String>(); 
			celFuncs = celular.getFuncsdocel();
			int location = 0;
			while(location < celFuncs.size()) {
				ps = connection.prepareStatement("insert into FuncDoCelular values ((select cod_cel from Celular where nome_cel = ?), (select cod_func from Funcionalidade where nome_func = ?))");
				ps.setString(1, celular.getModelo());
				ps.setString(2, celFuncs.get(location));
				ps.execute();
				location++;
			}
			ConexaoBanco.fecharConexao();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public List<Celular> buscar() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("select C.cod_cel, F.nome_fab, C.nome_cel, C.tela_cel, C.foto, C.flash from Celular C, Fabricante F where C.cod_fab = F.cod_fab");
			ResultSet rs = ps.executeQuery();
			List<Celular> celulares = new ArrayList<Celular>();
			while(rs.next()) {
				Celular celular = new Celular();
				celular.setCodigo(rs.getInt("cod_cel"));
				celular.setFabricante(rs.getString("nome_fab"));
				celular.setModelo(rs.getString("nome_cel"));
				celular.setTela(rs.getDouble("tela_cel"));
				celular.setFoto(rs.getString("foto"));
				celular.setFlash(rs.getString("flash"));
				celulares.add(celular);
			}
			ConexaoBanco.fecharConexao();
			return celulares;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
	public List<SelectItem> listcel() {
		try {
			Connection connection = ConexaoBanco.getConnection();
			PreparedStatement ps;
			ps = connection.prepareStatement("Select nome_cel from Celular");
			ResultSet rs = ps.executeQuery();
			List<SelectItem> listcel = new ArrayList<SelectItem>();
			while(rs.next()) {
				listcel.add(new SelectItem(rs.getString("nome_cel")));
			}
			ConexaoBanco.fecharConexao();
			return listcel;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}

	}
	
}
